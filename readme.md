# About

This package was made because I needed a simple way to live host a static site or VUE.JS site without having to use NGINX or IIS on my PC.  
This is similar to live server and nodemon.  
  
It is more of live-server (vs code live server) + nodemon + rsync all in one

# How to install

```
npm i -g host-this
```

# How to use

Once this package is referenced in your path, you can use one of the 2 commands to activate it from within a directory you want to host:

 - hostthis
 - host-this


To host a SPA website from the current directory  
```
hostthis -S
```` 

To host a site - a basic site  
```
hostthis
````

To host a site (and auto-reload on file changes) - a basic site  
```
hostthis -D
````

To host a SPA site (and auto-reload on file changes) - Single page website
```
hostthis -S -D
````

To host a basic file server
```
hostthis -F
````

Then go to http://localhost:3000/ to access your site

# Use a different port

```
hostthis -p 8080
``` 


# CLI Help

```
hostthis --help
``` 