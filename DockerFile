FROM node:lts-alpine

RUN npm install -g host-this

EXPOSE 8080

WORKDIR /app

CMD [ "host-this", "docker", ]