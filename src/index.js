const watcher = require('./watcher');
const server = require('./server');
const copyChange = require('./copy');
const execCmd = require('./exec');
const path = require('path');
const openPath = require('open');
const colors = require('colors');

const cleanSafePath = (dir) => {
  dir = dir || process.cwd();
  if (dir.indexOf('//') < 0 && dir.indexOf(':/') < 0 && dir.indexOf('/') !== 0)
    dir = path.join(process.cwd(), dir);
  return dir;
}

module.exports = async function(
  spa = false,
  dev = false,
  web = false,
  webfileserver = false,
  host = 'localhost',
  port = 3000,
  dir,
  webdir,
  devdir,
  copy = false,
  watch = [/.*/g],
  ignore = [/\/[.].+\//g],
  exec = false,
  open = false,
  verbose = false) {
  dir = cleanSafePath(dir);
  webdir = cleanSafePath(webdir);
  devdir = cleanSafePath(devdir);

  if (web !== false) {
    server.start(host, port, spa, dev, webdir, webfileserver, verbose);
  }
  let watchedChanges = () => {
    console.error('This feature is only available in dev mode...')
  }
  if (dev !== false) {
    let alreadyBusy = false;
    let holdingChanges = [];
    watchedChanges = async (changes) => {
      if (alreadyBusy) {
        return holdingChanges = holdingChanges.concat(changes);
      }
      console.log('Changes detected.'.bgBlue.white);
      alreadyBusy = true;
      if (web)
        server.forceBusyForRefresh();

      if (exec !== false && typeof exec === 'string') {
        let stOut = await execCmd(exec, verbose);
        if (verbose)
          console.warn(stOut);
        else
          console.warn(`Exec ran successfully`.bgGreen.white);
      }

      if (copy !== false && typeof copy === 'string')
        for (let change of changes)
          await copyChange(dir, copy, change.event, change.file, change.stat);

      if (web)
        server.forceRefreshForAllClients();

      alreadyBusy = false;
      if (holdingChanges.length > 0) {
        let tHoldingChanges = holdingChanges;
        holdingChanges = [];
        watchedChanges(holdingChanges);
      }
    };
    watcher(devdir, watch, ignore, true, verbose, watchedChanges);
  }

  if (open !== false) {
    if (web === false) {
      console.warn(`Opening ... ${open}`.bgBlue.white);
      await openPath(open);
    } else {
      console.warn(`Opening ... http://${host}:${port}/`.bgBlue.white);
      await openPath(`http://${host}:${port}/`);
    }
  }

  return {
    reload: () => {
      console.log('Forced reload... trying...'.bgBlack.red)
      watchedChanges([]);
    }
  }
};