const {
  exec
} = require('child_process');
const colors = require('colors');

module.exports = function(execCmd, verbose) {
  return new Promise((resolve, reject) => {
    if (verbose)
      console.warn(`RUN CMD: ${execCmd}`.bgGreen.white);
    else
      console.warn(`Running exec command`.bgGreen.white);
    exec(execCmd, (err, stdout, stderr) => {
      if (err) return reject(err, stderr, stdout);
      resolve(stdout);
    });
  })
}