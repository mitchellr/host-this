const path = require('path');
const chokidar = require('chokidar');
const colors = require('colors');

module.exports = function(cwd, watch, ignore, bulk = false, verbose = false, callback = () => {}) {
  let bulkChanges = [];
  let bulkTimer = null;
  const bulkCallback = () => {
    let tBChanges = bulkChanges;
    bulkChanges = [];
    bulkTimer = null;
    callback(tBChanges);
  };
  chokidar.watch(cwd, {
    ignored: [path.join(cwd, 'node_modules'), path.join(cwd, '.git'), path.join(cwd, 'bin')],
    persistent: true
  }).on('all', (event, file, stat) => {
	let tFileLinux = file.replace(/\\/g, '/');
    for (let ignoreEx of ignore) {
      if (ignoreEx.test(tFileLinux)) return;
    }
    for (let watchEx of watch) {
      if (!watchEx.test(tFileLinux)) return;
    }
    if (verbose)
      console.log(`[Change Event] ${event.toUpperCase()}:${file}`.bgBlue.white);
    if (bulk) {
      if (bulkTimer !== null) {
        clearTimeout(bulkTimer);
      }
      bulkTimer = setTimeout(bulkCallback, 1000);
      bulkChanges.push({
        event,
        file,
        stat
      });
      return;
    }
    callback(event, file, stat);
  });
}