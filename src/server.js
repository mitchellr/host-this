const mime = require("mime");
const path = require("path");
const fs = require("fs");
const colors = require("colors");

const getMimeType = (path) => {
  if (path.endsWith(".vue")) return "application/javascript";
  return mime.getType(path);
};

let preBusySend = true;
let listOfConnectedClients = [];
const forceRefreshForAllClients = () => {
  preBusySend = false;
  console.log("Forcing web UI to refresh".bgGreen.white);
  for (let i = 0; i < listOfConnectedClients.length; i++) {
    listOfConnectedClients[i].conn.socket.send("refresh");
  }
};
const forceBusyForRefresh = () => {
  preBusySend = true;
  console.log("Notifying web UI of changes".bgBlue.white);
  for (let i = 0; i < listOfConnectedClients.length; i++) {
    listOfConnectedClients[i].conn.socket.send("pre-refresh");
  }
};
const getFileInfoAsync = (reqPath) =>
  new Promise((resovle, reject) =>
    fs.stat(reqPath, (err, stats) => (err ? reject(err) : resovle(stats)))
  );

const devInjectCode =
  '<script>let htmlToNotifyPreChanges = \'<div style="position: fixed; top: 10px; left: 10px; right: 10px; width: calc(100% - 60px); z-index: 9999999999999; background: #a50000; text-align: center; padding: 20px; color: #FFFFFF; border-radius: 5px;">Change detected. We will reload shortly.</div>\'; const echoSocketUrl = `ws://${window.location.hostname}:${window.location.port}/ws_connect`;const socket = new WebSocket(echoSocketUrl);socket.onmessage = e => {if (e.data === "pre-refresh"){ document.getElementsByTagName("body")[0].innerHTML = htmlToNotifyPreChanges + document.getElementsByTagName("body")[0].innerHTML; } if (e.data === "refresh"){location.reload(true);}}</script>';
const generateAutoReloadCode = (content) => {
  if (content.indexOf("</body") < 0)
    console.warn("INJECT CODE NOT ADDED TO RESPONSE!".yellow);
  else content = content.replace("</body", `${devInjectCode}</body`);
  return content;
};

const statusPageHtml = (
  statusCode,
  message,
  indectCode = true,
  reply,
  specMsg = null
) => {
  reply.code(statusCode);
  reply.header("Content-Type", "text/html");
  return `<!DOCTYPE html><html><head><title>${
    specMsg ? specMsg.title : `HTTP ${statusCode}`
  }</title><style>html,body { height: 100%; margin: 0; padding: 0; width: 100%; } body { display: table; } .my-block { text-align: center; display: table-cell; vertical-align: middle; }</style></head><body><div class="my-block">${
    specMsg
      ? specMsg.html
      : `HTTP ${statusCode}<br />${message}<br /><br /><a href="/">click here to go home</a>`
  }</div>${indectCode ? devInjectCode : ""}</body></html>`;
};

let fastify;
const funcs = {
  async setup(verbose) {
    fastify = require("fastify")({
      logger: verbose,
    });
    await fastify.register(require("@fastify/middie"));
    await fastify.register(require("@fastify/sensible"));
    await fastify.register(require("@fastify/websocket"));
    await fastify.register(require("@fastify/cors"), {
      origin: true,
      methods: ["GET"],
      allowedHeaders: "*",
    });

    console.log(`Setting up events server`.bgBlack.white);
    fastify.get(
      "/ws_connect",
      {
        websocket: true,
      },
      (connection /* SocketStream */, req /* FastifyRequest */) => {
        let thisKey = new Date().getTime();
        listOfConnectedClients.push({
          key: thisKey,
          conn: connection,
        });
        console.log(
          `Client Connected [${thisKey}] (/${listOfConnectedClients.length})`
            .bgBlack.white
        );
        connection.socket.on("close", (conn) => {
          for (let i = 0; i < listOfConnectedClients.length; i++) {
            if (listOfConnectedClients[i].key === thisKey) {
              listOfConnectedClients.splice(i, 1);
              break;
            }
          }
          console.log(
            `Client Disconnected [${thisKey}] (/${listOfConnectedClients.length})`
              .bgBlack.white
          );
        });
        if (preBusySend) connection.socket.send("pre-refresh");
      }
    );
  },
  async start(
    host,
    port,
    spa = false,
    dev = false,
    dir = process.cwd(),
    webfileserver = false,
    verbose = false
  ) {
    await funcs.setup(verbose);
    fastify.get(
      "/*",
      async (request, reply, next) =>
        new Promise(async (resolve, reject) => {
          if (dev && preBusySend) {
            return resolve(
              statusPageHtml(200, "", dev, reply, {
                title: "We're busy getting things ready",
                html: `We're busy doing stuffs for you, please wait...<br /><br />If it takes a while, then please check the console window for more info on what stuffs we are doing.`,
              })
            );
          }

          let requestUrl = request.url;
          let filePathLocal = path.join(dir, `.${requestUrl}`);
          if (verbose) console.log(`Readfile:${filePathLocal}`.bgBlack.white);
          if (!fs.existsSync(filePathLocal))
            return resolve(
              statusPageHtml(
                404,
                "Content/page/directory not found",
                dev,
                reply
              )
            );

          let fileInfo = fs.statSync(filePathLocal);
          if (verbose)
            console.log(
              `Readfile:${filePathLocal} is dir ${fileInfo.isDirectory()}`
                .bgBlack.white
            );
          if (fileInfo.isDirectory()) {
            if (webfileserver) {
              if (!requestUrl.endsWith("/")) requestUrl += "/";
              let directoryHtml = `<html><head><title>${requestUrl}</title></head><body>`;
              if (requestUrl != "/") {
                let pat = requestUrl
                  .split("/")
                  .slice(0, requestUrl.split("/").length - 2)
                  .join("/");
                directoryHtml += `<a href="${
                  pat.startsWith("/") ? pat : `/${pat}`
                }"> < [${pat.startsWith("/") ? pat : `/${pat}`}] BACK < </a>`;
              }
              directoryHtml += `<ul>`;
              let stuffInDirectory = await new Promise((resovle, reject) =>
                fs.readdir(filePathLocal, (err, stats) =>
                  err ? reject(err) : resovle(stats)
                )
              );
              for (let stuffItem of stuffInDirectory) {
                let stuffType = await getFileInfoAsync(
                  path.join(filePathLocal, stuffItem)
                );
                if (stuffType.isFile())
                  directoryHtml += `<li>[FILE] <a href="${requestUrl}${stuffItem}">${stuffItem}</a></li>`;
                else if (stuffType.isDirectory())
                  directoryHtml += `<li>[DIR] <a href="${requestUrl}${stuffItem}">${stuffItem}</a></li>`;
                else directoryHtml += `<li>[??] ${stuffItem}</li>`;
              }
              reply.header("Content-Type", "text/html");
              return resolve(
                `${directoryHtml}</ul>${dev ? devInjectCode : ""}</body></html>`
              );
            } else if (spa === true) {
              requestUrl =
                request.url +
                (request.url.endsWith("/") ? "" : "/") +
                "index.html";
              filePathLocal = path.join(dir, `.${requestUrl}`);
              if (verbose)
                console.log(`Try SPA:${filePathLocal}...`.bgBlack.white);
              if (!fs.existsSync(filePathLocal))
                return resolve(
                  statusPageHtml(
                    404,
                    "Content/page/directory not found",
                    dev,
                    reply
                  )
                );
            } else
              return resolve(
                statusPageHtml(
                  404,
                  "Content/page/directory not found",
                  dev,
                  reply
                )
              );
          }

          let relativeCheck = path.relative(process.cwd(), filePathLocal);
          if (relativeCheck.indexOf("../") === 0)
            return resolve(statusPageHtml(400, "Bad request", dev, reply));

          reply.header("Content-Type", getMimeType(filePathLocal));
          if (dev && requestUrl.endsWith(".html")) {
            return resolve(
              generateAutoReloadCode(
                fs.readFileSync(path.join(dir, `.${requestUrl}`), "utf8")
              )
            );
          }
          resolve(fs.createReadStream(filePathLocal));
        })
    );

    try {
      await fastify.listen({ port, host });
      console.log(
        `Web Server listening on http://${host}:${port}/`.bgGreen.white
      );
    } catch (err) {
      fastify.log.error(err);
      process.exit(1);
    }
  },
  forceRefreshForAllClients,
  forceBusyForRefresh,
};

module.exports = funcs;
