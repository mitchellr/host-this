const path = require('path');
const fs = require('fs');
const colors = require('colors');

module.exports = function(cwd, dest, event, file, stat) {
  return new Promise((resolve, reject) => {
    if (['change', 'add'].indexOf(event) >= 0) {
      // added/changed
      let newFile = path.join(dest, path.relative(cwd, file));
      console.log(`cp: ${file} => ${newFile}`.bgGreen.white);
      fs.copyFileSync(file, newFile);
    } else if (event === 'unlink') {
      // deleted
      //console.log(`File: ${y} was deleted. we're not touching it. sorry.`)
      if (fs.existsSync(newFile)) {
        fs.unlink(newFile);
      }
    } else if (event === 'addDir') {
      if (!fs.existsSync(newFile)) {
        fs.mkdirSync(newFile);
      }
    } else
      console.log(`${file}:${newFile}`.bgGreen.white);
    resolve();
  })
}