const sade = require("sade");
const pjson = require("../package.json");
const core = require("./index");
const colors = require("colors");

sade("host-this", true)
  .version(pjson.version)
  .describe("HOST THIS!")
  .example("-S -D")
  .example('-W false -D -e "touch ./test.txt"')
  .example(
    '-S -D -W -i "[.]vuepress/[.]cache/,[.]vuepress/[.]temp/,[.]vuepress/dist/" -B "docs/.vuepress/dist/" -R "docs/" -e "npm run build" -O false -C'
  )
  .example("my-app --dev")
  .option(
    "-S, --spa",
    "Server type [SPA - Single Page App] Redirects to /index.html",
    false
  )
  .option("-D, --dev", "Server type [DEV] Auto reloads on file changes", false)
  .option("-W, --web", "Enable/Disable web server for serving content", true)
  .option(
    "-F, --fileserver",
    "Enable/Disable web file server - directories return an html page with content",
    false
  )
  .option("-H, --host", "Hostname to bind", "localhost")
  .option("-p, --port", "Port to bind", 3000)
  .option("-d, --dir", "Directory to bind to", "./")
  .option("-B, --webdir", "Directory to bind web to (uses dir if not defined)")
  .option("-R, --devdir", "Directory to watch (uses dir if not defined)")
  .option("-c, --copy", "Copy on change to", false)
  .option("-V, --verbose", "Verbose logging", false)
  .option(
    "-w, --watch",
    "Watch changes to files (regex on file path) - comma seperated",
    ".*"
  )
  .option(
    "-i, --ignore",
    "Ignore changes to files (regex on file path) - comma seperated",
    "/[.].+/"
  )
  .option("-O, --open", "Open a URL/Path when started")
  .option("-e, --exec", "Run command on changes")
  .option(
    "-C, --consolereload",
    "Disable typing `r` in console to force-reload",
    false
  )
  .action(async (params, opts) => {
    console.log("Running host-this".white.bgGreen);
    // Program handler
    if (params.verbose) {
      console.log(params);
      console.log(opts);
    }
    let coreApp = await core(
      params.spa || false,
      params.dev || false,
      params.web || false,
      params.fileserver || false,
      params.host,
      params.port,
      params.dir,
      params.webdir || params.dir,
      params.devdir || params.dir,
      params.copy || false,
      params.watch.split(",").map((x) => new RegExp(x)),
      params.ignore.split(",").map((x) => new RegExp(x)),
      params.exec || false,
      params.open || false,
      params.verbose || false
    );

    if (params.consolereload !== true && params.web === true) {
      console.log(
        'You can type "r" and press enter to force reload this tool...'.bgBlack
          .bgYellow
      );
      var stdin = process.openStdin();
      stdin.addListener("data", function (d) {
        if (d.toString().trim().toLowerCase() === "r") {
          coreApp.reload();
        }
      });
    } else {
      process.openStdin();
    }
  })
  .parse(process.argv);
